<?php
/**
 * Custom functions
 */

add_theme_support( '-woocommerce' );

add_filter('woocommerce_show_page_title', '__return_false');
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

// Disable reviews from single tabs

add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);

function sb_woo_remove_reviews_tab($tabs){ 

	unset($tabs['reviews']); 
	
	return $tabs;
}