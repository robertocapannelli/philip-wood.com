// Modified http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
// Only fires on body class (working off strictly WordPress body_class)

var ExampleSite = {
  // All pages
  common: {
    init: function() {
      // JS here
$(window).load(function(){
      $('.-woocommerce select').selectpicker();
});
      
    },
    finalize: function() { }
  },
  // Home page
  home: {
    init: function() {
    
/*==== Show-hide discover on homepage ====*/
$(window).load(function(){
	$(".slidingDiv").hide();
	$(".show_hide").show();
	$('.show_hide').click(function(){
		$(".slidingDiv").slideToggle();
		});
	});
    $(".default_product_display:last-child, .featured-product:last-child").css({borderBottom:"none"}).hover(function(){$(this).addClass("last-child");},function(){$(this).removeClass("last-child");});}
  },
  
  
  // About page
  about: {
    init: function() {
      // JS here
    }
  }
};

var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = ExampleSite;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {

    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });

    UTIL.fire('common', 'finalize');
  }
};

$(document).ready(UTIL.loadEvents);
