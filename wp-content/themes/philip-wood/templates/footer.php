<footer class="content-info" role="contentinfo">

	<div class="container">
  
    	<div class="col-md-4">
    
			<?php dynamic_sidebar('sidebar-footer'); ?>
            
		</div>
		
		<div class="col-md-4">
			
			<h3><?php _e('Be Social','roots'); ?></h3>
			
			<ul>
				
				<li>
					
					<a href="https://plus.google.com/u/0/+Philipwood1/about">Google Plus</a>
							
				</li>
				
				<li>
					
					<a href="https://twitter.com/philipwoood">Twitter</a>
							
				</li>
				
				<li>
					
					<a href="http://www.pinterest.com/woodphilip/">Pinterest</a>
							
				</li>
				
				<li>
					
					<a href="http://instagram.com/philipwoood">Instagram</a>
							
				</li>
				
			</ul>
            
		</div>
		
		<div class="col-md-4" itemscope itemtype="http://schema.org/Organization">
    
			<h3><?php _e('Contatti','roots'); ?></h3>
				
				<span itemprop="name">Philip Wood</span>
				
				<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" >
				
					<span itemprop="addressLocality">Roma</span><br>
					
					Mobile: <span itemprop="telephone">3296036192</span>
					
				</div>
            
		</div>
    
		<?php /* <span class="shadow-top-to-bottom"></span>
	    
		<span class="shadow-bottom-to-top"></span> */?>
    
	</div>
	  
</footer>

<p style="clear:both;padding:10px;">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> - <?php _e('designed & developed by', 'roots'); ?> <a href="http://www.walkap.com">walkap.com</a> - <?php _e('photographed by', 'roots'); ?> <a href="http://emanuelemenduni.com" target="_blank">emanuelemenduni.com</a></p>

<?php wp_footer(); ?>
