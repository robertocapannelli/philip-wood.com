<div class="slider-wrapper">

	<div class="container">
    	
    	<div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="0" data-cycle-prev="#prev" data-cycle-next="#next">
		
    		<?php echo do_shortcode("[metaslider id=422]"); ?>
	
	</div>
    	
</div>