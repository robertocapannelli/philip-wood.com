<div class="top-bar">
	
	<div class="container">
	
		<div class="login-box">
		
			<?php if (is_user_logged_in()) {
			
					global $current_user; get_currentuserinfo(); ?>
					
        	        <?php _e('Hi', 'roots'); ?>, 
        	        	
        	        <?php if($current_user->user_firstname){
        	        			
        	        		echo $current_user->user_firstname . ' ' . $current_user->user_lastname;
        	        			
        	        }else{
        	        		
        	        		echo $current_user->user_login; } 
        	        		
        	        global $woocommerce; ?>
        	        			
        	        - <a href="<?php echo get_permalink(433)?> ">
        	        	
        	        	<?php _e('My Account', 'roots')?> </a> | <a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logout"><?php _e('Logout', 'roots') ?></a>
        	                    
			<?php } else { ?>
        	                 
        		<a href="<?php echo get_permalink(433) ?>"><?php _e('Sign in or Sign Up', 'roots') ?></a>
        	                    
			<?php } ?>
			
		</div>
		
		<?php if (is_user_logged_in()) { ?>
		
		<div class="cart-box">
			
			<span class="cart-contents">
						
				<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'roots'); ?>">
						
							<?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'roots'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
							
			</span>
					
			<?php 
							
				add_filter('add_to_cart_fragments', 'woocommerceframework_header_add_to_cart_fragment');

				function woocommerceframework_header_add_to_cart_fragment( $fragments ) {
							
				global $woocommerce;
	
				ob_start(); 
			
			?>
							
			<span class="cart-contents">
					
				<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'roots'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'roots'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
						
			</span>
					
			<?php
	
				$fragments['span.cart-contents'] = ob_get_clean();
	
				return $fragments; 
				
				} 
				
			?>
			
		</div>
		
		<?php } ?>
		
	</div>
		
</div>

<header class="banner navbar navbar-default navbar-static-top" role="banner">

	<div class="container">
    	<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<h1><a class="navbar-brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a></h1>
		</div>

		<nav class="collapse navbar-collapse" role="navigation">
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
        endif;
      ?>
      
	  	</nav>
    
  </div>
  
</header>
