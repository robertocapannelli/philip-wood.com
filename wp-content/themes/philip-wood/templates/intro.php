<section class="intro full">
	
	<div class="container">
	    
	        <div class="box-intruduction col-md-4">
	        
	            <h3><?php echo get_post_meta($post->ID, 'wpcf-left-box-title', true); ?></h3>
	            
	            <p><?php echo get_post_meta($post->ID, 'wpcf-left-box-description', true); ?></p>
	            
	        </div>
	        
	        <div class="box-intruduction col-md-4">
	        
	            <h3><?php echo get_post_meta($post->ID, 'wpcf-center-box-title', true); ?></h3>
	            
	            <p><?php echo get_post_meta($post->ID, 'wpcf-center-box-description', true); ?></p>
	            
	        </div>
	        
	        <div class="box-intruduction col-md-4">
	        
	            <h3><?php echo get_post_meta($post->ID, 'wpcf-right-box-title', true); ?></h3>
	            
	            <p><?php echo get_post_meta($post->ID, 'wpcf-right-box-description', true); ?></p>
	            
	        </div>
	        
	        <h2 class="claim"><?php _e('Philip loves and respects nature','roots'); ?></h2>
	        
	        <?php /*<div class="request">
	        
	        	<div class="form-wrapper">
	        	
					<?php echo do_shortcode('[gravityform id="3" name="Richiesta" title="false" description="false" ajax="true"]'); ?>
	    		
	        	</div>
	        	
			</div> /? ?>
	    	
	    </div>
	
	
	<?php /*
	<a href="javascript:void(0)" class="show_hide button" style="display:none;"><?php _e('Discover','roots'); ?></a>
	
	<div class="slidingDiv">
	
	    <div class="container">
	    
	        <div class="box-intruduction col-md-4">
	        
	            <h3><?php echo get_post_meta($post->ID, 'wpcf-left-box-title', true); ?></h3>
	            
	            <p><?php echo get_post_meta($post->ID, 'wpcf-left-box-description', true); ?></p>
	            
	        </div>
	        
	        <div class="box-intruduction col-md-4">
	        
	            <h3><?php echo get_post_meta($post->ID, 'wpcf-center-box-title', true); ?></h3>
	            
	            <p><?php echo get_post_meta($post->ID, 'wpcf-center-box-description', true); ?></p>
	            
	        </div>
	        
	        <div class="box-intruduction col-md-4">
	        
	            <h3><?php echo get_post_meta($post->ID, 'wpcf-right-box-title', true); ?></h3>
	            
	            <p><?php echo get_post_meta($post->ID, 'wpcf-right-box-description', true); ?></p>
	            
	        </div>
	        
	        <h2 class="claim"><?php _e('Philip loves and respects nature','roots'); ?></h2>
	        
	        <?php the_content() ?>
	    	
	    </div>
	    
	    <?php /* <span class="shadow-top-to-bottom"></span>
	    
	    <span class="shadow-bottom-to-top"></span> 
	    
	    <div class="container">
	    
	    	<div class="form-wrapper">
	    
	    <?php echo do_shortcode('[gravityform id="3" name="Richiesta" title="false" description="false" ajax="true"]'); ?>
	    
	    	</div>
	    
	    </div>
	    
	</div>
	
	*/?>
	    
</section>