<?php /* Template Name: Front Page */ ?>

<?php get_template_part('templates/slider'); ?>

<?php get_template_part('templates/intro'); ?>

<?php /*<div class="featured-products-wrapper">

	<div class="container">
	
		<?php
			$featured_product = new WP_Query( array(
				'post_type' => 'product',
				//'meta_key' => 'featured_products',
				//'meta_value' => 'yes',
				'posts_per_page' => '3'
			) );
			
			$i = 0;
			
			if ($featured_product->have_posts()) : while ($featured_product->have_posts()) : $featured_product->the_post();
			
				if ($i%2){
					$classe = "even";
					}else{
						$classe = "odd";
					}
				$i++; ?>
							
	            	<article class="featured-product <?php echo $classe; ?>" itemscope itemtype="http://schema.org/Product">
	            	
	            	<?php if (has_post_thumbnail()): ?>
	                         
	                    <div class="image-col" itemprop="image">
	                	              
	                		<a rel="bookmark" href="<?php echo get_permalink( $product->ID ); ?>" title="<?php echo get_the_title( $product->ID ); ?>">
	                	                  
					    		<?php echo woocommerce_get_product_thumbnail(); ?>
					    				  	
					    	</a>
	                	                  
	                	</div>
	                              
					<?php else: ?>
	                          
	                	<div class="item_no_image">
	                	          
	                	    <a href="<?php echo get_the_title( $product->ID ); ?>">
	                	            
	                	        <span><?php _e('No Image Available','roots'); ?></span>
	                	            	
					    	</a>
	                	            
	                	</div>
	                          
					<?php endif; ?>
	                          
	                    <div class="description-col">
	                              
	                        <h2 itemprop="name">
	                                  
	                        	<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'roots' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	                                      
	                        </h2> 
	                                                                   
	                        <div itemprop="description">
	                                  
	                        	<?php the_excerpt(); ?>
	                                  	
	                        </div>
	                                  
	                        <a href="<?php the_permalink(); ?>" class="button" rel="bookmark"><?php _e('Find out','roots'); ?></a>
	                                  
	                    </div>
	                              
	                </article>
	                           
				<?php endwhile;else:?>
				
				
					<?php //nothing ?>
				
				
				<?php endif; ?>
		
	</div> <!-- .container -->

</div> <!-- .featured-products-wrapper -->*/ ?>